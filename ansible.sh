#/bin/#!/usr/bin/env bash

ansible-galaxy install -r requirements.yml
ansible-playbook playbook_aws.yml --ask-vault-pass --tags create_ec2
ansible-playbook playbook_aws.yml --ask-vault-pass
ansible-playbook playbook_runner.yml --ask-vault-pass --tags create_ec2
echo "	To remove ec2 instances run following playbooks with tag:"
echo " 	--------------------------------------------------------"
echo "ansible-playbook playbook_runner.yml --ask-vault-pass --tags delete_ec2"
echo "ansible-playbook playbook_aws.yml --ask-vault-pass --tags delete_ec2"
