Link to repository with application and pipeline:\
https://gitlab.com/karol44m/sentry \
Pipeline is defined in `.gitlab-ci.yml` file.\
All secrets for pipeline are inside gitlab.

## Install Ansible and the EC2 module dependencies.
for Debian family systems:\
`sudo apt-get install -y python python-pip ansible`\
for EL family systems:\
`sudo yum install -y python python-pip ansible`\
for both:\
`pip install boto boto3`

## Create SSH keys to connect to the EC2 instance after provisioning.
`ssh-keygen -t rsa -b 4096 -f ~/.ssh/mykey`
## Create Ansible Vault file to store the AWS Access and Secret keys.
`ansible-vault create group_vars/all/pass.yml`
## Edit the pass.yml file and create the keys global constants.
`ansible-vault edit group_vars/all/pass.yml`
add your credentials:
>ec2_access_key: AAAAAABBBBBB  
>ec2_secret_key: afjzzzzzzzzzzzzzzzzzgjsfdbtirhf

## Run bash script to provision Sentry and gitlab-runners on AWS.
`./ansible.sh`

## Playbooks for deleting AWS instances
`ansible-playbook playbook_aws.yml --ask-vault-pass --tags delete_ec2` \
`ansible-playbook playbook_runner.yml --ask-vault-pass --tags delete_ec2`
